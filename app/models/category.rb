class Category < ActiveRecord::Base
  has_attached_file :image, styles: { medium: "242x200>", thumb: "100x100>" }, default_url: "/img/noimage.jpg"
  has_many :subcategories

  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  validates :name, presence: true, uniqueness: { case_sensitive: false }

  before_destroy :set_default_category

  scope :with_subcategory, -> { joins(:subcategories).where('COUNT(subcategories) > ?', 0) }

  private
    def set_default_category
      if cat = Category.find_by_name('Default Category')
      	default_category = cat
      else
      	default_category = Category.create(name: 'Default Category')
      end
      Subcategory.where(category_id: self.id).each do |sc|
      	sc.update_attribute(:category_id, default_category.id)
      end
    end
end
