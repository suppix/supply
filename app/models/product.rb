class Product < ActiveRecord::Base
  belongs_to :subcategory
  belongs_to :supplier
  has_attached_file :photo, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/img/noimage.jpg"
  has_many :order_items
  has_many :characteristics, dependent: :destroy

  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\Z/
  validates :name, presence: true
  validates :measurement, presence: true

  def self.create_characteristics product, characteristics, values
    if characteristics != nil
    	characteristics.each do |key_c, value_c|
        if (value_c != '') && (value_c != nil)
      		value_arr = []
      	  values.each do |key_v, value_v|
      	  	if key_c == key_v.split('_')[0]
      	  		value_arr.push(value_v)
      	  	end
      	  end
      	  characteristic = Characteristic.create(name: value_c, product_id: product.id) if value_arr.count > 0
      	  value_arr.each do |v|
            if (v != '') && (v != nil)
      	  	  Value.create(name: v, characteristic_id: characteristic.id)
            end
      	  end
          characteristic.destroy unless characteristic.values.any?
        end
    	end
    end
  end

  def self.update_characteristics product, characteristics, values
    if characteristics != nil
      characteristics.each do |key_c, value_c|
        characteristic = Characteristic.find_by_id(key_c)
        characteristic.update_attribute(:name, value_c) if characteristic
      end
      values.each do |key_v, value_v|
        value = Value.find_by_id(key_v)
        value.update_attribute(:name, value_v) if value
      end
      characteristics.each do |key_c, value_c|
        c = Characteristic.find_by_id(key_c)
        if c
          c.destroy unless c.values.any?
        end
      end
    end
  end

  def self.remove_products_by_supplier supplier
    supplier.products.each do |p|
      p.update_attribute(:remove, true)
    end
  end

  def self.search_product search
    where("LOWER(name) LIKE LOWER(?)", "%#{search}%") 
    where("LOWER(description) LIKE LOWER(?)", "%#{search}%")
  end
end
