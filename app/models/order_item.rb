class OrderItem < ActiveRecord::Base
  belongs_to :order
  belongs_to :product

  validates :quantity, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :product_id, presence: true
  validates :order_id, presence: true

  def self.get_items item_ids
    order_items = OrderItem.where("id IN (#{item_ids.gsub(' ',',')})")
  end

  def self.update_quantity item_id, quantity
    OrderItem.find(item_id).update_attribute(:quantity, quantity)
  end

  def self.update_sended_status item_id
    OrderItem.find(item_id).update_attribute(:sended, true)
  end

  def self.update_sended_date item_id
    OrderItem.find(item_id).update_attribute(:date_sended, Time.now)
  end

  def self.get_item_with_params order_id, product_id, characteristics
    OrderItem.where("order_id = ? AND product_id = ? AND characteristics = ? AND sended = ?", order_id, product_id, characteristics.to_s, "false")
  end
end
