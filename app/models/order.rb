class Order < ActiveRecord::Base
  belongs_to :user
  belongs_to :order_status
  has_many :order_items, dependent: :destroy

  validates :order_status_id, :presence => { :message => 'parameter values are not valid'}
  validates :user_id,         :presence => { :message => 'parameter values are not valid'}

  before_validation :set_order_status

  def self.create_and_change(params)
    order = create(user_id: params[:user].id, address: params[:address], message: params[:message])

    params[:cart].items.each do |cart|
      cart.each do |cart_item|
        quantity = params[:items]["product_id_#{cart_item.id}#{cart_item.current_item(params[:cart])}"]
        characteristics = cart_item.characteristics

        order.order_items.create(product_id:      cart_item.product.id,
                                 quantity:        quantity,
                                 characteristics: characteristics)
      end
    end
    order
  end

  def self.create_or_update_items product, quantity, order, characteristics
    item = OrderItem.get_item_with_params(order.id, product.id, characteristics)
    if item[0]
      quantity = item[0].quantity.to_i + quantity.to_i
      item[0].update_attribute(:quantity, quantity)
    else
      order.order_items.create(product_id: product.id, quantity: quantity, characteristics: characteristics)
    end
  end

  def self.check_order_status_after_send order
    case order.order_items.map(&:sended).uniq.join(',').to_s
      when 'true,false'
        OrderStatus.find_by_name('Half handled')
      when 'false,true'
        OrderStatus.find_by_name('Half handled')
      when 'true'
        OrderStatus.find_by_name('Handled')
      else
        OrderStatus.find_by_name('New')
      end
  end

  private
	  def set_order_status
	    self.order_status_id ||= 1
	  end
end
