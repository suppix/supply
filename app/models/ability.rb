class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    
    if user.has_role? :admin or user.has_role? :manager
      can :manage, :all

      #can :access, :rails_admin
      #if user.has_role? :admin
      #  can :manage, :all
      #elsif user.has_role? :manager
      #  can [:index, :show, :edit, :destroy], [Order, OrderItem, Category, Subcategory, Product, Characteristic, Value, Supplier]
      #  can [:index], User
      #  can [:index], OrderStatus
      #  can :dashboard
      #end
    elsif user.has_role? :user
      can :read, :all
      can :create, Order
      cannot [:index, :show, :update, :send_order, :remove_item, :add_item], Order
      cannot :manage, Category
      cannot :manage, Subcategory
      cannot :manage, Product
      cannot :manage, Supplier
      cannot :manage, User
      cannot :manage, OrderItem
    end
  end
end
