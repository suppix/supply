class Characteristic < ActiveRecord::Base
  belongs_to :product
  has_many   :values, dependent: :destroy

  def self.get_characteristics characteristics
  	char = {}
    if characteristics != nil
      characteristics.each do |k, v|
        if (characteristic = Characteristic.find(k).name) && (value = Value.find(v).name)
          char[characteristic] = value
        end
      end
    end
    return char
  end
end
