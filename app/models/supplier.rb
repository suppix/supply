class Supplier < ActiveRecord::Base
  has_many :products

  validates :name,    presence: true
  validates :address, presence: true
  validates :email,   presence: true

  def self.get_suppliers items
  	suppliers = []
  	items.each do |i|
  		suppliers.push(i.product.supplier)
  	end
  	suppliers.uniq
  end
end
