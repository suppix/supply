class User < ActiveRecord::Base
  rolify
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :orders

  before_create :notify_administrator
  after_create :set_user_role

  def notify_administrator
    User.all.each do |u|
      if u.has_role? :admin
        NotificationsMailer.create_user(self, u).deliver
      end
    end
  end

  def set_user_role
    self.add_role :user
  end

  def self.update_role role, user
    user.remove_role :admin
    user.remove_role :manager
    user.remove_role :user
    user.add_role role
  end
end
