class Subcategory < ActiveRecord::Base
  belongs_to :category
  has_many :products
  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/img/noimage.jpg"

  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  validates :name, presence: true

  before_destroy :set_default_category

  private
    def set_default_category
      if cat = Category.find_by_name('Default Category')
        default_category = cat
      else
        default_category = Category.create(name: 'Default Category')
      end

      if subcat = Subcategory.find_by_name('Default Subcategory')
      	default_subcategory = subcat
      else
      	default_subcategory = Subcategory.create(name: 'Default Subcategory', category_id: default_category.id)
      end
      Product.where(subcategory_id: self.id).each do |p|
      	p.update_attribute(:subcategory_id, default_subcategory.id)
      end
    end
end
