class Cart
  attr_reader :session

  def initialize(session)
    @session = session
  end

  def items
    session.map do |id, params|
      params.map do |k, v|
        CartItem.new(Product.find(id), v)
      end
    end
  end

  def total
    items.map { |item| item.total }.inject(&:+)
  end

  def remove_item(remove_item_param, current_user)

    id              = remove_item_param['product_id']
    quantity        = remove_item_param["product_params"]["quantity"]
    characteristics = remove_item_param["product_params"]["characteristics"]

    if characteristics == nil
      characteristics = {}
    end

    product_params = {"quantity" => quantity, "characteristics" => characteristics}

    if id && product_params
      session[id].delete(session[id].key(product_params))
      if session[id] == {}
        session.delete(id)
      end
    end
    @user_cart = UserCart.find_by_user_id(current_user.id)
    if @user_cart
      @user_cart.update_attributes(:cart_hash => session)
    end
    session
  end

  def update(carts_param, current_user)
    id              = carts_param[:product_id]
    quantity        = carts_param[:quantity]
    new_item        = true
    characteristics = {}

    Characteristic.where(product_id: id).all.each do |c|
      characteristics["#{c.id}"] = carts_param["characteristic_#{c.id}"] if carts_param["characteristic_#{c.id}"]
    end

    if (session != {}) && (session[id])
      session[id].each do |k, v|
        if v['characteristics'] == characteristics
          v['quantity'] = "#{v['quantity'].to_i + quantity.to_i}"
          new_item = false
        end
      end
      if new_item
        session[id][session[id].length] = { 'quantity' => quantity, 'characteristics' => characteristics }
      end
    else
      session[id] = { 0 => { 'quantity' => quantity, 'characteristics' => characteristics } }
    end
    @user_cart = UserCart.find_by_user_id(current_user.id)
    if @user_cart
      @user_cart.update_attributes(:cart_hash => session)
    end
    session
  end

  def destroy(current_user)
    @user_cart = UserCart.find_by_user_id(current_user.id)
    if @user_cart
      @user_cart.update_attributes(cart_hash: '{}')
    end
    session = {}
  end

  def count
    session.present? ? "(#{calculate_count})" : nil
  end

  def empty?
    items.empty?
  end

private
  def calculate_count
    count = 0
    session.values.each do |v|
      v.values.each do |q|
        count = count + q['quantity'].to_i
      end
    end
    return count
  end
end