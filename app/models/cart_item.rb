class CartItem
  attr_reader :product, :params

  def initialize(product, params)
    @product = product
    @params  = params
  end

  def name
    product.name
  end

  def id
    product.id
  end

  def measurement
    product.measurement
  end

  def percent_off
    product.percent_off
  end

  def on_sale?
    product.on_sale?
  end

  def quantity
    params['quantity']
  end

  def characteristics
    char = {}
    params['characteristics'].each do |k, v|
      if (characteristic = Characteristic.find(k).name) && (value = Value.find(v).name)
        char[characteristic] = value
      end
    end
    return char
  end

  def product_params
    params
  end

  def current_item cart
    cart.session["#{product.id}"].key(params)
  end
end