class NotificationsMailer < ActionMailer::Base
  helper MailerHelper
	default from: "officetrident@gmail.com",
          template_path: "mailers"

  def create_order_user order
    @order = order
    @user  = User.find(@order.user_id)

    mail(:to => @user.email, :subject => "Order created!")
  end

  def create_order_admin order, user
  	@order = order
  	@user  = user

  	mail(:to => @user.email, :subject => "Order created!")
  end

  def send_order_other_email address, item_ids, supplier_id, user, other_email
    @supplier = Supplier.find(supplier_id)
    @items    = OrderItem.get_items(item_ids)
    @address  = address
    @user     = user
    @email    = other_email
    mail(:to => @email, :subject => "#{@supplier.name} has received a new order!")
  end

  def send_order_supplier address, item_ids, supplier_id, user
    @supplier = Supplier.find(supplier_id)
    @items    = OrderItem.get_items(item_ids)
    @address  = address
    @user     = user
    mail(:to => @supplier.email, :subject => "You've received a new order!")
  end

  def create_user user, admin
    @user  = user
    @admin = admin

    mail(:to => @admin.email, :subject => "A new user has beed added!")
  end

  def activate_user_account user
    @user  = user

    mail(:to => @user.email, :subject => "Your account has been activated!")
  end
end
