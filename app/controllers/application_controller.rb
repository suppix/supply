class ApplicationController < ActionController::Base
  include CanCan::ControllerAdditions
  protect_from_forgery with: :exception

  before_action :authenticate_user!
  before_filter :check_current_user

  helper_method :current_cart

  def current_cart
    @cart ||= Cart.new(session[:cart])
  end

  def find_or_create_cart
    if session[:cart]
      session[:cart]
    elsif UserCart.find_by_user_id(current_user.id)
      session[:cart] = Hash.new(eval(UserCart.find_by_user_id(current_user.id).cart_hash))
    else
      session[:cart] = Hash.new(0)
      UserCart.create(user_id: current_user.id, cart_hash: session[:cart])
    end
  end

  def check_current_user
    if current_user && current_user.active_account == false
      sign_out current_user 
    end
  end

  def after_sign_in_path_for(resource)
    @user_cart = UserCart.find_by_user_id(current_user.id)
    if @user_cart
      session[:cart] = eval(@user_cart.cart_hash)
    end
    super
  end

  rescue_from CanCan::AccessDenied do | exception |
    redirect_to root_url, alert: exception.message
  end
end
