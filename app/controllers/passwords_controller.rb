class PasswordsController < Devise::PasswordsController
  protected
  def after_sending_reset_password_instructions_path_for(resource_name)
  	flash[:change_password] = "The instruction of changing your password has been sent to your email!"
    super
  end
end