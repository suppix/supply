class OrdersController < ApplicationController
  before_action :set_order, only: [:show, :send_order, :resend_order]
  load_and_authorize_resource

  def index
    respond_to do |format|
      format.html
      format.json { render json: OrderDatatable.new(view_context) }
    end
  end

  def show
    @suppliers = Supplier.get_suppliers @order.order_items
    @products  = Product.where(remove: false).all
    @product   = Product.where(remove: false).find(params[:product_id]) if params[:product_id]
  end

  def create
    @order = Order.create_and_change(cart:    current_cart,
                                     user:    current_user,
                                     items:   params[:items],
                                     address: params[:address],
                                     message: params[:message])

    NotificationsMailer.create_order_user(@order).deliver
    User.all.each do |u|
      if u.active_account == true
        if u.has_role? :admin or u.has_role? :manager
          NotificationsMailer.create_order_admin(@order, u).deliver
        end
      end
    end

    if @order.valid?
      session[:cart] = current_cart.destroy(current_user)
      redirect_to root_path(order: 'create'),
        :notice => "Order submitted!"
    else
      redirect_to cart_path, :notice => "Checkout failed."
    end
  end

  def remove_item
    @order_item = OrderItem.find(params[:item_id])
    @order = @order_item.order
    @order_item.destroy
    status = Order.check_order_status_after_send(@order)
    @order.update_attribute(:order_status_id, status.id)
    redirect_to :back
  end

  def add_item
    order           = Order.find(params[:order_id])
    product         = Product.find(params[:product_id])
    quantity        = params[:quantity]
    characteristics = Characteristic.get_characteristics(params[:characteristics])
    Order.create_or_update_items(product, quantity, order, characteristics) if product && order && characteristics
    redirect_to :back
  end

  def send_order
    params[:items].split(' ').each do |i|
      OrderItem.update_quantity(i, params["item_id_#{i}"])
      OrderItem.update_sended_status(i)
      OrderItem.update_sended_date(i)
    end
    status = Order.check_order_status_after_send(@order)
    @order.update_attribute(:order_status_id, status.id)
    NotificationsMailer.send_order_supplier(params[:address], params[:items], params[:supplier_id], @order.user).deliver
    if params[:other_email].present?
      emails = params[:other_email].to_s.gsub(' ','').split(',')
      emails.each do |e|
        NotificationsMailer.send_order_other_email(params[:address], params[:items], params[:supplier_id], @order.user, e).deliver
      end
    end
    redirect_to :back
  end

  def resend_order
    NotificationsMailer.send_order_supplier(params[:address], params[:items], params[:supplier_id], @order.user).deliver
    if params[:other_email].present?
      emails = params[:other_email].to_s.gsub(' ','').split(',')
      emails.each do |e|
        NotificationsMailer.send_order_other_email(params[:address], params[:items], params[:supplier_id], @order.user, e).deliver
      end
    end
    redirect_to :back
  end

  private
    def set_order
      @order = Order.find(params[:id])
    end
end
