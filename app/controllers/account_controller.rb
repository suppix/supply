class AccountController < ApplicationController
  before_filter :find_or_create_cart

  def profile
  end

  def orders_list
  	@orders = Order.where(user_id: current_user.id).order('created_at DESC').all.page(params[:page]).per(12)
  end

  def cart
  end
end
