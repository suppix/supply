class SubcategoriesController < ApplicationController
  before_action :set_subcategory, only: :show

  def show
    @subcategories = Subcategory.where(category_id: @subcategory.category_id).order('created_at DESC').all
    @products      = @subcategory.products.where(remove: false).order('created_at DESC').all.page(params[:page]).per(12)
  end

  private
    def set_subcategory
      @subcategory = Subcategory.find(params[:id])
    end
end
