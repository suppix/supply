class DashboardController < ApplicationController
  skip_authorize_resource  only: :thanks_for_signing_up
  skip_authorization_check only: :thanks_for_signing_up

  def home
    if !current_user
      redirect_to new_user_session_path
    end
  	@categories = Category.order('created_at DESC').all.page(params[:page]).per(12)
  end

  def thanks_for_signing_up
  end
end
