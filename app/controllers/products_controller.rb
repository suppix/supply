class ProductsController < ApplicationController
  before_action :set_product, only: :show

  def show
  	if @product.remove == true
  	  redirect_to :back
  	end
  end

  def search
    @products = Product.search_product(params[:search])
    @products = @products.page(params[:page]).per(12)
  end

  private
    def set_product
      @product = Product.find(params[:id])
    end
end
