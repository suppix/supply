class Admin::ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy, :restore]

  load_and_authorize_resource
  respond_to :html

  def index
    respond_to do |format|
      format.html
      format.json { render json: Admin::ProductsDatatable.new(view_context) }
    end
  end

  def show
    respond_with(@product)
  end

  def new
    @subcategories = Subcategory.all
    @suppliers     = Supplier.all
    respond_with(@product)
  end

  def edit
    @subcategories = Subcategory.all
    @suppliers     = Supplier.all
  end

  def create
    @product = Product.new(product_params)
    @product.save
    Product.create_characteristics(@product, params['characteristic'], params['value'])
    redirect_to admin_product_path(@product.id)
  end

  def update
    @product.update(product_params)
    Product.create_characteristics(@product, params['characteristic'], params['value'])
    Product.update_characteristics(@product, params['edit_characteristic'], params['edit_value'])
    redirect_to edit_admin_product_path(@product.id)
  end

  def remove_characteristic
    @characteristic = Characteristic.find(params[:characteristic_id])
    @characteristic.destroy

    respond_to do |format|
      format.html { redirect_to :back }
      format.json { head :no_content }
      format.js   { render :layout => false }
    end
  end

  def remove_value
    Value.find(params[:value_id]).destroy
    redirect_to :back
  end

  def destroy
    @product.update_attribute(:remove, true)
    redirect_to admin_products_path
  end

  def restore
    @product.update_attribute(:remove, false)
    redirect_to admin_products_path
  end

  private
    def set_product
      @product = Product.find(params[:id])
    end

    def product_params
      params.require(:product).permit(:name, :photo, :measurement, :subcategory_id, :supplier_id, :description)
    end
end
