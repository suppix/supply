class Admin::SuppliersController < ApplicationController
  before_action :set_supplier, only: [:show, :edit, :update, :destroy]

  load_and_authorize_resource
  respond_to :html

  def index
    respond_to do |format|
      format.html
      format.json { render json: Admin::SuppliersDatatable.new(view_context) }
    end
  end

  def show
    respond_with(@supplier)
  end

  def new
    @supplier = Supplier.new
    respond_with(@supplier)
  end

  def edit
  end

  def create
    @supplier = Supplier.new(supplier_params)
    @supplier.save
    redirect_to admin_supplier_path(@supplier.id)
  end

  def update
    @supplier.update(supplier_params)
    redirect_to edit_admin_supplier_path(@supplier.id)
  end

  def destroy
    @supplier.update_attribute(:remove, true)
    Product.remove_products_by_supplier @supplier
    redirect_to admin_suppliers_path
  end

  def restore
    @supplier.update_attribute(:remove, false)
    redirect_to admin_products_path
  end

  private
    def set_supplier
      @supplier = Supplier.find(params[:id])
    end

    def supplier_params
      params.require(:supplier).permit(:name, :email, :address, :phone)
    end
end
