class Admin::CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]

  load_and_authorize_resource
  respond_to :html

  def index
    respond_to do |format|
      format.html
      format.json { render json: Admin::CategoriesDatatable.new(view_context) }
    end
  end

  def show
    respond_with(@category)
  end

  def new
    @category = Category.new
    respond_with(@category)
  end

  def edit
  end

  def create
    @category = Category.new(category_params)
    @category.save
    redirect_to admin_category_path(@category.id)
  end

  def update
    @category.update(category_params)
    redirect_to edit_admin_category_path(@category.id)
  end

  def destroy
    @category.destroy
    redirect_to admin_categories_path
  end

  private
    def set_category
      @category = Category.find(params[:id])
    end

    def category_params
      params.require(:category).permit(:name, :image)
    end
end
