class Admin::UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  load_and_authorize_resource
  respond_to :html

  def index
    respond_to do |format|
      format.html
      format.json { render json: Admin::UsersDatatable.new(view_context) }
    end
  end

  def show
    respond_with(@user)
  end

  def new
    @user = User.new
    respond_with(@user)
  end

  def edit
  	@roles = Role.all
  end

  def create
    @user = Category.new(user_params)
    @user.save
    redirect_to admin_user_path(@user.id)
  end

  def update
    @status = @user.active_account
    @user.update(user_params)
    User.update_role params[:role], @user
    if (@user.active_account == true) && (@status == false)
      NotificationsMailer.activate_user_account(@user).deliver
    end
    redirect_to edit_admin_user_path(@user.id)
  end

  def destroy
    @user.destroy
    redirect_to admin_users_path
  end

  private
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:email, :first_name, :last_name, :phone, :active_account)
    end
end
