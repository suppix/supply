class Admin::SubcategoriesController < ApplicationController
  before_action :set_subcategory, only: [:show, :edit, :update, :destroy]

  load_and_authorize_resource
  respond_to :html

  def index
    respond_to do |format|
      format.html
      format.json { render json: Admin::SubcategoriesDatatable.new(view_context) }
    end
  end

  def show
    respond_with(@subcategory)
  end

  def new
    @subcategory = Subcategory.new
    @categories  = Category.all
    respond_with(@subcategory)
  end

  def edit
  	@categories = Category.all
  end

  def create
    @subcategory = Subcategory.new(subcategory_params)
    @subcategory.save
    redirect_to admin_subcategory_path(@subcategory.id)
  end

  def update
    @subcategory.update(subcategory_params)
    redirect_to edit_admin_subcategory_path(@subcategory.id)
  end

  def destroy
    @subcategory.destroy
    redirect_to admin_subcategories_path
  end

  private
    def set_subcategory
      @subcategory = Subcategory.find(params[:id])
    end

    def subcategory_params
      params.require(:subcategory).permit(:name, :image, :category_id)
    end
end
