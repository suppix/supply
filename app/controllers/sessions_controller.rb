class SessionsController < Devise::SessionsController
	 def new
    super
  end

  def create
    user = User.find_by_email(params[:user][:email])

    if !user or !user.valid_password?(params[:user][:password])
    	flash[:not_active] = "Invalid Email or Password!"
    elsif user.active_account == false
      flash[:not_active] = "Your account hasn't been activated yet!"
    end

    self.resource = warden.authenticate!(auth_options)
    set_flash_message(:notice, :signed_in) if is_flashing_format?
    sign_in(resource_name, resource)

    yield resource if block_given?

    respond_with resource, location: after_sign_in_path_for(resource)
  end
end
