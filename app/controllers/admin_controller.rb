class AdminController < ApplicationController
  load_and_authorize_resource
  layout 'admin'
  def home
  	@categories = Category.all
  	@subcategories = Subcategory.all
  	@products = Product.where(remove: false).all
  	@orders = Order.all
  	@suppliers = Supplier.all
  	@users = User.all
  end
end
