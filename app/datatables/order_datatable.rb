class OrderDatatable < ApplicationDatatable
  include AccountHelper

  def sortable_columns
    @sortable_columns ||= %w(Order.id
                             User.email
                             Order.created_at
                             OrderStatus.name)
  end

  def searchable_columns
    @searchable_columns ||= %w(Order.id
                               User.email
                               Order.created_at
                               OrderStatus.name)
  end

  private

  def data
    records.map do |record|
      [
        record.id,
        user(record.user),
        date(record.created_at),
        status(record.order_status),
        action(record.id)
      ]
    end
  end

  def get_raw_records
    Order.joins(:user, :order_status)
  end

  def date(date)
    date.strftime('%m.%d.%y %H:%M:%S')
  end

  def status(status)
    content_tag(:span) do
      content_tag(:label, status.name.upcase, class: "label #{get_color_status(status.id)}")
    end
  end

  def user(user)
    user.first_name + ' ' + user.last_name
  end

  def action(order_id)
    content_tag(:span) do
      link_to('Show', order_path(order_id))
    end
  end
end
