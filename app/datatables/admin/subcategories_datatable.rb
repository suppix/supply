class Admin::SubcategoriesDatatable < ApplicationDatatable

  def sortable_columns
    @sortable_columns ||= %w(Subcategory.name
                             Category.name)
  end

  def searchable_columns
    @searchable_columns ||= %w(Subcategory.name
                               Category.name)
  end

  private

  def data
    records.map do |record|
      [
        name(record),
        category(record),
        image(record),
        actions(record)
      ]
    end
  end

  def get_raw_records
    Subcategory.joins(:category)
  end

  def category(subcategory)
    content_tag(:span) do
      link_to(subcategory.category.name, admin_category_path(subcategory.category.id))
    end
  end

  def name(subcategory)
    content_tag(:span) do
      link_to(subcategory.name, admin_subcategory_path(subcategory.id))
    end
  end

  def image(subcategory)
    content_tag(:span, class: 'image_field paperclip_type col-md-7 col-xs-7') do
      image_tag(subcategory.image.url(:thumb), class: 'img-thumbnail')
    end
  end

  def actions(subcategory)
    content_tag(:ul, class: 'inline list-inline') do
      content_tag(:li, class: 'icon show_member_link', rel: 'tooltip', title: 'Show') do
        link_to admin_subcategory_path(subcategory.id) do
          content_tag(:div, class: 'pjax') do
            content_tag(:div, '', class: 'glyphicon glyphicon-eye-open')
          end
        end
      end +
      content_tag(:li, class: 'icon edit_member_link', rel: 'tooltip', title: 'Edit') do
        link_to edit_admin_subcategory_path(subcategory.id) do
          content_tag(:div, class: 'pjax') do
            content_tag(:div, '', class: 'glyphicon glyphicon-pencil')
          end
        end
      end +
      content_tag(:li, class: 'icon delete_member_link', rel: 'tooltip', title: 'Delete') do
        link_to admin_subcategory_path(subcategory.id), method: :delete, data: { confirm: 'Are you sure?' } do
          content_tag(:div, class: 'pjax') do
            content_tag(:div, '', class: 'glyphicon glyphicon-remove')
          end
        end
      end
    end
  end
end
