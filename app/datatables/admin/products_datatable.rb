class Admin::ProductsDatatable < ApplicationDatatable

  def sortable_columns
    @sortable_columns ||= %W(Product.name
                             Subcategory.name
                             Supplier.name)
  end

  def searchable_columns
    @searchable_columns ||= %W(Product.name
                               Subcategory.name
                               Supplier.name)
  end

  private

  def data
    records.map do |record|
      [
        name(record),
        photo(record),
        subcategory(record),
        supplier(record),
        actions(record)
      ]
    end
  end

  def get_raw_records
    Product.where(remove: false).joins(:subcategory, :supplier)
  end

  def name(product)
    content_tag(:span) do
      link_to(product.name, admin_product_path(product.id))
    end
  end

  def photo(product)
    content_tag(:span) do
      image_tag(product.photo.url(:thumb), class: 'img-thumbnail')
    end
  end

  def subcategory(product)
     content_tag(:span) do
      link_to(product.subcategory.name, admin_subcategory_path(product.subcategory.id))
    end
  end

  def supplier(product)
    product.supplier.name
  end

  def actions(product)
    content_tag(:ul, class: 'inline list-inline') do
      content_tag(:li, class: 'icon show_member_link', rel: 'tooltip', title: 'Show') do
        link_to admin_product_path(product.id) do
          content_tag(:div, class: 'pjax') do
            content_tag(:div, '', class: 'glyphicon glyphicon-eye-open')
          end
        end
      end +
      content_tag(:li, class: 'icon edit_member_link', rel: 'tooltip', title: 'Edit') do
        link_to edit_admin_product_path(product.id) do
          content_tag(:div, class: 'pjax') do
            content_tag(:div, '', class: 'glyphicon glyphicon-pencil')
          end
        end
      end +
      content_tag(:li, class: 'icon delete_member_link', rel: 'tooltip', title: 'Delete') do
        link_to admin_product_path(product.id), method: :delete, data: { confirm: 'Are you sure?' } do
          content_tag(:div, class: 'pjax') do
            content_tag(:div, '', class: 'glyphicon glyphicon-remove')
          end
        end
      end
    end
  end
end