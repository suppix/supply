class Admin::SuppliersDatatable < ApplicationDatatable

  def sortable_columns
    @sortable_columns ||= %W(Supplier.name
                             Supplier.email
                             Supplier.address)
  end

  def searchable_columns
    @searchable_columns ||= %W(Supplier.name
                               Supplier.email
                               Supplier.address
                               Supplier.phone)
  end

  private

  def data
    records.map do |record|
      [
        name(record),
        record.email,
        record.address,
        record.phone,
        actions(record)
      ]
    end
  end

  def get_raw_records
    Supplier.where(remove: false)
  end

  def name(supplier)
    content_tag(:span) do
      link_to(supplier.name, admin_supplier_path(supplier.id))
    end
  end

  def actions(supplier)
    content_tag(:ul, class: 'inline list-inline') do
      content_tag(:li, class: 'icon show_member_link', rel: 'tooltip', title: 'Show') do
        link_to admin_supplier_path(supplier.id) do
          content_tag(:div, class: 'pjax') do
            content_tag(:div, '', class: 'glyphicon glyphicon-eye-open')
          end
        end
      end +
      content_tag(:li, class: 'icon edit_member_link', rel: 'tooltip', title: 'Edit') do
        link_to edit_admin_supplier_path(supplier.id) do
          content_tag(:div, class: 'pjax') do
            content_tag(:div, '', class: 'glyphicon glyphicon-pencil')
          end
        end
      end +
      content_tag(:li, class: 'icon delete_member_link', rel: 'tooltip', title: 'Delete') do
        link_to admin_supplier_path(supplier.id), method: :delete, data: { confirm: 'Are you sure?' } do
          content_tag(:div, class: 'pjax') do
            content_tag(:div, '', class: 'glyphicon glyphicon-remove')
          end
        end
      end
    end
  end
end