class Admin::UsersDatatable < ApplicationDatatable

  def sortable_columns
    @sortable_columns ||= %W(User.email
                             User.first_name
                             User.last_name
                             User.phone
                             User.active_account)
  end

  def searchable_columns
    @searchable_columns ||= %W(User.email
                               User.first_name
                               User.last_name
                               User.phone
                               Role.name)
  end

  private

  def data
    records.map do |record|
      [
        email(record),
        record.first_name,
        record.last_name,
        record.phone,
        record.orders.count,
        active_account(record),
        roles(record),
        actions(record)
      ]
    end
  end

  def get_raw_records
    User.joins(:roles).all
  end

  def email(user)
    content_tag(:span) do
      link_to(user.email, admin_user_path(user.id))
    end
  end

  def active_account(user)
    user.active_account ? content_tag(:span, '✓', class: 'label label-success') : content_tag(:span, '✘', class: 'label label-danger')
  end

  def roles(user)
    user.roles.map(&:name).join(',')
  end

  def actions(user)
    content_tag(:ul, class: 'inline list-inline') do
      content_tag(:li, class: 'icon show_member_link', rel: 'tooltip', title: 'Show') do
        link_to admin_user_path(user.id) do
          content_tag(:div, class: 'pjax') do
            content_tag(:div, '', class: 'glyphicon glyphicon-eye-open')
          end
        end
      end +
      content_tag(:li, class: 'icon edit_member_link', rel: 'tooltip', title: 'Edit') do
        link_to edit_admin_user_path(user.id) do
          content_tag(:div, class: 'pjax') do
            content_tag(:div, '', class: 'glyphicon glyphicon-pencil')
          end
        end
      end
    end
  end
end