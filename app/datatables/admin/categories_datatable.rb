class Admin::CategoriesDatatable < ApplicationDatatable

  def sortable_columns
    @sortable_columns ||= %w(Category.name)
  end

  def searchable_columns
    @searchable_columns ||= %w(Category.name)
  end

  private

  def data
    records.map do |record|
      [
        name(record),
        image(record),
        actions(record)
      ]
    end
  end

  def get_raw_records
    Category.all
  end

  def name(category)
    content_tag(:span) do
      link_to(category.name, admin_category_path(category.id))
    end
  end

  def image(category)
    content_tag(:span, class: 'image_field paperclip_type col-md-7 col-xs-7') do
      image_tag(category.image.url(:thumb), class: 'img-thumbnail')
    end
  end

  def actions(category)
    content_tag(:ul, class: 'inline list-inline') do
      content_tag(:li, class: 'icon show_member_link', rel: 'tooltip', title: 'Show') do
        link_to admin_category_path(category.id) do
          content_tag(:div, class: 'pjax') do
            content_tag(:div, '', class: 'glyphicon glyphicon-eye-open')
          end
        end
      end +
      content_tag(:li, class: 'icon edit_member_link', rel: 'tooltip', title: 'Edit') do
        link_to edit_admin_category_path(category.id) do
          content_tag(:div, class: 'pjax') do
            content_tag(:div, '', class: 'glyphicon glyphicon-pencil')
          end
        end
      end +
      content_tag(:li, class: 'icon delete_member_link', rel: 'tooltip', title: 'Delete') do
        link_to admin_category_path(category.id), method: :delete, data: { confirm: 'Are you sure?' } do
          content_tag(:div, class: 'pjax') do
            content_tag(:div, '', class: 'glyphicon glyphicon-remove')
          end
        end
      end
    end
  end
end
