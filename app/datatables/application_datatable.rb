class ApplicationDatatable < AjaxDatatablesRails::Base
  include AjaxDatatablesRails::Extensions::WillPaginate
  include Rails.application.routes.url_helpers
  def_delegator :@view, :link_to
  def_delegator :@view, :truncate
  def_delegator :@view, :l
  def_delegator :@view, :image_tag
  def_delegator :@view, :content_tag
end
