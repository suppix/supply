#= require dataTables/jquery.dataTables
#= require dataTables/extras/dataTables.responsive

$ ->
  table = $('#suppliers-table')
  table.DataTable
    processing: true
    serverSide: true
    ajax: Routes.admin_suppliers_path()
    columnDefs: [{
      bSortable: false,
      aTargets: [ 3, 4 ]
    }],
    columns: [
      { className: 'name_field string_type col-md-2 col-xs-2' },
      { className: 'name_field string_type col-md-2 col-xs-2' },
      { className: 'name_field string_type col-md-2 col-xs-2' },
      { className: 'name_field string_type col-md-2 col-xs-2' },
      { className: 'last links col-md-1 col-xs-1' }
    ]