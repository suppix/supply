#= require dataTables/jquery.dataTables
#= require dataTables/extras/dataTables.responsive

$ ->
  table = $('#users-table')
  table.DataTable
    processing: true
    serverSide: true
    ajax: Routes.admin_users_path()
    columnDefs: [{
      bSortable: false,
      aTargets: [ 4, 6, 7 ]
    }],
    columns: [
      { className: 'name_field string_type col-md-2 col-xs-2' },
      { className: 'name_field string_type col-md-1 col-xs-1' },
      { className: 'name_field string_type col-md-1 col-xs-1' },
      { className: 'name_field string_type col-md-1 col-xs-1' },
      { className: 'name_field string_type col-md-1 col-xs-1' },
      { className: 'active_account_field boolean_type col-md-1 col-xs-1' },
      { className: 'name_field string_type col-md-1 col-xs-1' },
      { className: 'last links col-md-1 col-xs-1' }
    ]