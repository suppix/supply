#= require dataTables/jquery.dataTables
#= require dataTables/extras/dataTables.responsive

$ ->
  table = $('#subcategories-table')
  table.DataTable
    processing: true
    serverSide: true
    ajax: Routes.admin_subcategories_path()
    columnDefs: [{
      bSortable: false,
      aTargets: [ 2, 3 ]
    }],
    columns: [
      { className: 'name_field string_type col-md-2 col-xs-2' },
      { className: 'name_field string_type col-md-2 col-xs-2' },
      { className: 'image_field paperclip_type col-md-5 col-xs-5' },
      { className: 'last links col-md-1 col-xs-1' }
    ]