#= require dataTables/jquery.dataTables
#= require dataTables/extras/dataTables.responsive

$ ->
  table = $('#categories-table')
  table.DataTable
    processing: true
    serverSide: true
    ajax: Routes.admin_categories_path()
    columnDefs: [{
      bSortable: false,
      aTargets: [ 1, 2 ]
    }],
    columns: [
      { className: 'name_field string_type col-md-2 col-xs-2' },
      { className: 'image_field paperclip_type col-md-7 col-xs-7' },
      { className: 'last links col-md-1 col-xs-1' }
    ]