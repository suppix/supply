#= require dataTables/jquery.dataTables
#= require dataTables/extras/dataTables.responsive

$ ->
  table = $('#products-table')
  table.DataTable
    processing: true
    serverSide: true
    ajax: Routes.admin_products_path()
    columnDefs: [{
      bSortable: false,
      aTargets: [ 1, 4 ]
    }],
    columns: [
      { className: 'name_field string_type col-md-2 col-xs-2' },
      { className: 'image_field paperclip_type col-md-2 col-xs-2' },
      { className: 'name_field string_type col-md-2 col-xs-2' },
      { className: 'name_field string_type col-md-4 col-xs-4' },
      { className: 'last links col-md-1 col-xs-1' }
    ]