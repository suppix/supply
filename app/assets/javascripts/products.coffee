# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
(($) ->
  $('.spinner .btn:first-of-type').on 'click', ->
    $('.spinner input').val parseInt($('.spinner input').val(), 10) + 1
    return
  $('.spinner .btn:last-of-type').on 'click', ->
    $('.spinner input').val parseInt($('.spinner input').val(), 10) - 1
    return
  return
) jQuery