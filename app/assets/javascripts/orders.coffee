#= require dataTables/jquery.dataTables
#= require dataTables/extras/dataTables.responsive

$ ->
  $(document).on 'change', '#select-new-product', (evt) ->
    $.ajax '',
      type: 'GET'
      dataType: 'script'
      data: {
        product_id: $("#select-new-product option:selected").val()
      }

      $('#add-item-button').prop 'disabled', false

      error: (jqXHR, textStatus, errorThrown) ->
        console.log("AJAX Error: #{textStatus}")
      success: (data, textStatus, jqXHR) ->
        console.log("Dynamic country select OK!")

  $(document).on 'change', '#search-order-by-user', (evt) ->

    $.ajax '',
      type: 'GET'
      dataType: 'script'
      data: {
        user_id: $("#search-order-by-user option:selected").val()
      }

      $('#user_id').val $('#search-order').val()
      $('#search-product').submit()
      error: (jqXHR, textStatus, errorThrown) ->
        console.log("AJAX Error: #{textStatus}")
      success: (data, textStatus, jqXHR) ->
        console.log("Dynamic country select OK!")

  table = $('#orders-table')
  table.DataTable
    processing: true
    serverSide: true
    ajax: Routes.orders_path()
    columnDefs: [{
      bSortable: false,
      aTargets: [ 4 ]
    }]
