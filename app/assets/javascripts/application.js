// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require dataTables/jquery.dataTables
//= require dataTables/bootstrap/3/jquery.dataTables.bootstrap
//= require jquery-ui
//= require bootstrap
//= require select2
//= require js-routes
//= require_tree .

$( document ).ready(function() {

  var offset = 60;
	$('.rounded a').click(function(event) {
    event.preventDefault();
    $($(this).attr('href'))[0].scrollIntoView();
    if ($(this).attr('href')[1] == 0) {
    	scrollBy(0, -offset);
    }
	});

  function formatState (state) {
    if (!state.id) { return state.text; }
    var element = "#image_" + state.element.value;
    var url = $(element).val();
    var $state = $(
      '<span><img src="' + url + '" height="40" width="40" class="margin-right-10"/>'+ state.text +'</span>'
    );
    return $state;
  };

  $('#add_product').select2({
    templateResult: formatState
  });

  //$("#add_product").remove(1);

  $('#search-order').select2({
    allowClear: true,
    placeholder: "Select a user"
  });

  $('#deliver-to-address').keyup(function() {
    var address = $('#deliver-to-address').val();
    $('#hidden-address').val(address);
  });

  $('#other-email').keyup(function() {
    var other_email = $('#other-email').val();

    $('#hidden-other-email').val(other_email);
    $('#hidden-resend-other-email').val(other_email);
    if (other_email.includes(" ") && other_email.includes(", ") == false) {
      $('#separate-emails').removeClass('hide');
    } else if ($('#separate-emails').hasClass('hide') == false){
      $('#separate-emails').addClass('hide');
    }
  });

  $('#add-item-button').click(function() {
    $('#add-new-item').submit();
  });

  $('.ajax-delete-characteristic').click(function() {
    if (confirm('Are you sure?')) {
      $(this).prev().click();
      $(this).closest('.characteristic-cart').fadeOut();
    }
  });

  $('.ajax-delete-value').click(function() {
    $(this).closest('.value-cart').fadeOut();
  });

  var add_characteristic     = $(".add_characteristic_button");
  var wrapper_characteristic = add_characteristic.closest(".input_fields_characteristic");
  $(add_characteristic).click(function(){
    lastParrentId = parseInt($(this).closest('#product_characteristics_field').find('.qwerty').last().attr('data-item'))
    id = lastParrentId + 1
    console.log(lastParrentId)

    $(wrapper_characteristic).append('<div class="qwerty" data-item="' + id + '" style="margin-bottom: 15px;">\
                                        <div class="col-sm-6">\
                                          <div class="controls">\
                                            <input type="text" name="characteristic[' + id+ ']" id="characteristic_" value="" class="form-control">\
                                          </div>\
                                        </div>\
                                        <div class="col-sm-5 input_fields_value">\
                                          <div class="input-group"  style="margin-bottom: 15px;">\
                                            <input class="form-control" name="value[' + id + '_1]" data-item="1" type="text">\
                                            <span class="input-group-addon add_value_button">\
                                              <div class="glyphicon glyphicon-plus"></div>\
                                            </span>\
                                          </div>\
                                        </div>\
                                        <script></script>\
                                        <span class="col-sm-1 padding-0 remove_characteristic">\
                                          <div class="btn btn-default">\
                                            <div class="glyphicon glyphicon-remove"></div>\
                                          </div>\
                                        </span>\
                                        <div class="clearfix"></div>\
                                      </div>');
  });
  $(wrapper_characteristic).on("click",".remove_characteristic ", function(){
    $(this).parent('div').remove();
  })

  $(".input_fields_characteristic").on('click', '.add_value_button', function(){
    qwerty = $(this).closest(".input_fields_value")
    parrentDiv = $(this).closest(".qwerty");
    lastParrentId = parseInt(parrentDiv.attr('data-item'));
    lastInputId = parseInt(parrentDiv.find('.input-group').last().find('.form-control').attr('data-item'));
    id = lastInputId + 1
    nameId = lastParrentId + '_' + (lastInputId + 1);
    console.log(nameId )
    qwerty.append('<div class="input-group" style="margin-bottom: 15px;">\
                    <input name="value[' + nameId + ']"  data-item="' + id + '" class="form-control" type="text">\
                    <span class="input-group-addon remove_value">\
                      <div class="glyphicon glyphicon-remove"></div>\
                    </span>\
                  </div>');
  });
  $(".input_fields_characteristic").on("click",".remove_value", function(){
    $(this).closest('.input-group').remove();
  })
});
