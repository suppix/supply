module AccountHelper
  def get_color_status id
  	status = OrderStatus.find(id).name
  	case status
  	  when 'New'
        'label-danger'
  	  when 'Handled'
  	  	'label-success'
  	  when 'Half handled'
  	  	'label-warning'
  	  when 'Done'
  	  	'label-default'
  	  else
  	  	'label-danger'
  	  end
  end
end
