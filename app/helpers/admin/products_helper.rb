module Admin::ProductsHelper
	def get_characteristics characteristics
		char = []
    characteristics.each do |c|
      if characteristic = Characteristic.find(c)
        char.push(characteristic)
      end
    end
    return char
	end

	def get_values values
		val = []
    values.each do |v|
      if values = Value.find(v)
        val.push(values)
      end
    end
    return val
	end
end
