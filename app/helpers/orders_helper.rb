module OrdersHelper
  def quantity_of_products order
    quantity = 0
    order.order_items.each do |oi|
      quantity = quantity + oi.quantity
    end
    return quantity
  end
end
