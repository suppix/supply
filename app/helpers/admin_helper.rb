module AdminHelper
  def get_record_bar
  	values = []
  	cat = Category.all.count
  	sub = Subcategory.all.count
  	pro = Product.where(remove: false).all.count
  	ord = Order.all.count
  	sup = Supplier.all.count
  	usr = User.all.count
  	records = [cat, sub, pro, ord, sup, usr]
    records.each do |i|
    	val = (i.to_i * 100)/records.max.to_i
      val = i if val == 0
      values.push(val)
    end
    return values
  end
end
