module OrderItemsHelper

  def get_order_item order, supplier
  	product = []
  	order.order_items.each do |oi|
  	  if oi.product.supplier == supplier
  	  	product.push(oi)
  	  end
  	end
  	product
  end

  def get_item id
  	OrderItem.find(oi)
  end

  def check_sended_items items
  	items.map(&:sended).uniq[0]
  end

  def check_items items
    case items.map(&:sended).uniq
      when [true, false]
        'additional_order'
      when [true]
        'true'
      else
        'false'
      end
  end

  def get_sended_order_items items
    order_items = []
    items.each do |oi|
      if oi.sended == true
        order_items.push(oi)
      end
    end
    order_items
  end

  def get_not_sended_order_items items
    order_items = []
    items.each do |oi|
      if oi.sended == false
        order_items.push(oi)
      end
    end
    order_items
  end

  def get_date_sended items
  	items.map(&:date_sended).uniq[0].to_datetime
  end
end
