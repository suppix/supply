class AddSupplierToProducts < ActiveRecord::Migration
  def change
  	add_column :products, :supplier_id, :integer
  	add_index  :products, :supplier_id
  end
end
