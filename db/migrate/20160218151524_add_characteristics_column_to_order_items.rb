class AddCharacteristicsColumnToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :characteristics, :text
  end
end
