class AddSendColumnToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :sended, :boolean, default: false, null: false
  end
end
