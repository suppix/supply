class CreateUserCarts < ActiveRecord::Migration
  def change
    create_table :user_carts do |t|
      t.integer :user_id
      t.text :cart_hash

      t.timestamps null: false
    end
  end
end
