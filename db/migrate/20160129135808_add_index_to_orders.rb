class AddIndexToOrders < ActiveRecord::Migration
  def change
  	add_index :orders,      :order_status_id
  	add_index :order_items, :order_id
  	add_index :order_items, :product_id
  end
end
