class AddImageToSubcategories < ActiveRecord::Migration
  def up
    add_attachment :subcategories, :image
  end

  def down
    remove_attachment :subcategories, :image
  end
end
