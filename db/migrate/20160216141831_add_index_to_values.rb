class AddIndexToValues < ActiveRecord::Migration
  def change
  	add_index :values, :characteristic_id
  end
end
