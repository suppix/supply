class AddColumnsToProductsAndSuppliers < ActiveRecord::Migration
  def change
  	add_column :products,  :remove, :boolean, default: false, null: false
  	add_column :suppliers, :remove, :boolean, default: false, null: false
  end
end
