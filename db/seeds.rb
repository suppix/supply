if Rails.env.development?
	User.delete_all
	user_1 = User.create(email: 								 'demouser1@supply.com',
												first_name: 					 'User_1',
												last_name:             'Demo',
												phone:                 '0987654321',
												active_account:         true,
												password:              '12345678',
												password_confirmation: '12345678')
	user_1.add_role 'user'

	user_2 = User.create(email: 								 'demouser2@supply.com',
												first_name: 					 'User_2',
												last_name:             'Demo',
												phone:                 '0987654321',
												active_account:         true,
												password:              '12345678',
												password_confirmation: '12345678')
	user_2.add_role 'user'

	user_admin = User.create(email: 						 'demoadmin@supply.com',
												first_name: 					 'Admin',
												last_name:             'Demo',
												phone:                 '0987654321',
												active_account:         true,
												password:              '12345678',
												password_confirmation: '12345678')
	user_admin.add_role 'admin'

	user_manager = User.create(email: 					 'demomanager@supply.com',
												first_name: 					 'Manager',
												last_name:             'Demo',
												phone:                 '0987654321',
												active_account:         true,
												password:              '12345678',
												password_confirmation: '12345678')
	user_manager.add_role 'manager'

	Supplier.delete_all
	supplier_1 = Supplier.create(name: 'Metro',     address: 'IF', email: 'metro@supply.com',     phone: '0987654321')
	supplier_2 = Supplier.create(name: 'Epicenter', address: 'IF', email: 'epicenter@supply.com', phone: '0987654321')
	supplier_3 = Supplier.create(name: 'Arsen',     address: 'IF', email: 'arsen@supply.com',     phone: '0987654321')

	Category.delete_all
	Subcategory.delete_all
	Product.delete_all
	Characteristic.delete_all
	Value.delete_all
	5.times do |c|
		category = Category.create(name: "Category_#{c}", 
			                         image: File.open(File.join(Rails.root, "/public/img/lumber.jpg")))
	  5.times do |s|
	  	subcategory = Subcategory.create(name: "Subcategory_#{c}#{s}", 
	  		                               category_id: category.id, 
	  		                               image: File.open(File.join(Rails.root, "/public/img/doska.jpg")))
	  	5.times do |p|
	  	  product = Product.create(name: "Product_#{c}#{s}#{p}",  
	  	  	                       measurement: 'm2', 
	  	  	                       subcategory_id: subcategory.id, 
	  	  	                       supplier_id: Supplier.order("RANDOM()").first.id, 
	  	  	                       photo: File.open(File.join(Rails.root, "/public/img/doska.jpg")),
	  	  	                       description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
	  	  	                                       Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet,
	  	  	                                       consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	  	  	                                       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.')
	  	  3.times do |char|
	  	  	charactiristic = Characteristic.create(name: "char_#{c}#{s}#{p}#{char}", product_id: product.id)

	  	  	3.times do |v|
	  	  		value = Value.create(name: "values_#{c}#{s}#{p}#{char}#{v}", characteristic_id: charactiristic.id)
	  	  	end
	  	  end
	  	end
	  end
	end

	OrderStatus.delete_all
	order_status_1 = OrderStatus.create(id: 1, name: "New")
	order_status_2 = OrderStatus.create(id: 2, name: "Handled")
	order_status_3 = OrderStatus.create(id: 3, name: "Half handled")
	order_status_4 = OrderStatus.create(id: 4, name: "Done")

	Order.delete_all
	OrderItem.delete_all
	10.times do |i|
	  order = Order.create(order_status_id: OrderStatus.first.id, 
	  	                   address: '700 M St,Fresno, CA 93721, USA',
	  	                   user_id: User.order("RANDOM()").first.id)
	  5.times do |i|
	  	product = Product.order("RANDOM()").first
	  	characteristics = {}
	  	product.characteristics.each do |char|
	  		characteristics[char.name] = char.values.first.name
	  	end
	  	order_item = OrderItem.create(product_id: product.id, 
	  								  							order_id: Order.order("RANDOM()").first.id, 
	  								  							quantity: Random.rand(1..10),
	  								  							characteristics: characteristics)
	  end
	end
end
