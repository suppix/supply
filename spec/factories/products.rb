FactoryGirl.define do
  factory :product do
  	sequence(:name) { |n|  "Product_#{n}" }
  	measurement 'm2'
  end
end
