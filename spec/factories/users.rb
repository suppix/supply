FactoryGirl.define do
  factory :user do
    sequence(:email) { |n|  "user#{n}@email.com" }
    password '11111111'
    password_confirmation '11111111'
    first_name 'Test'
    last_name 'User'
  end
end
