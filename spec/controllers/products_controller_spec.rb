require 'rails_helper'

RSpec.describe ProductsController, type: :controller do
  
  let(:valid_attributes) { { name: 'product_1', subcategory_id: '1', measurement: 'm2' } }
  let(:valid_session)    { {} }

  describe 'GET #show' do
    context 'when the user is logged in' do
      it "assigns the requested product as @product" do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        user = FactoryGirl.create(:user)
        sign_in user
        product = Product.create! valid_attributes

        get :show, {:id => product.to_param}, valid_session
        expect(assigns(:product)).to eq(product)
      end
    end

    context 'when the user is NOT logged in' do
      it 'redirects them to the log in page' do
        product = Product.create! valid_attributes

        get :show, {:id => product.to_param}, valid_session
        expect(response).to redirect_to new_user_session_path
      end
    end
  end
end
