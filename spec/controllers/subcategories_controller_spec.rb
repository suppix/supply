require 'rails_helper'

RSpec.describe SubcategoriesController, type: :controller do

  let(:valid_attributes) { { name: 'category_1', category_id: '1'} }
  let(:valid_session)    { {} }

  describe 'GET #show' do
    context 'when the user is logged in' do
      it "assigns the requested product as @subcategory" do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        user = FactoryGirl.create(:user)
        sign_in user
        subcategory = Subcategory.create! valid_attributes

        get :show, {:id => subcategory.to_param}, valid_session
        expect(assigns(:subcategory)).to eq(subcategory)
      end
    end

    context 'when the user is NOT logged in' do
      it 'redirects them to the log in page' do
        subcategory = Subcategory.create! valid_attributes
        
        get :show, {:id => subcategory.to_param}, valid_session
        expect(response).to redirect_to new_user_session_path
      end
    end
  end
end
