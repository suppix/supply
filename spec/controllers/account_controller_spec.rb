require 'rails_helper'

RSpec.describe AccountController, type: :controller do

  describe 'profile' do
    context 'when the user is logged in' do
      it "returns http success" do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        user = FactoryGirl.create(:user)
        sign_in user
        get :profile
        expect(response).to have_http_status(:success)
      end
    end

    context 'when the user is NOT logged in' do
      it 'redirects them to the log in page' do
        get :profile
        expect(response).to redirect_to new_user_session_path
      end
    end
  end



  #describe "GET #orders_list" do
  #  before(:each) do
  #    @request.env["devise.mapping"] = Devise.mappings[:user]
  #    user = FactoryGirl.create(:user)
  #    sign_in user#
  #
  #    10.times { Order.create(user_id: subject.current_user.id) }
  #    10.times { Order.create }
  #  end
  #  it "returns http success" do
  #    get :orders_list
  #    assigns(:orders).should have(10).orders
  #  end
  #end

  describe "GET #cart" do
    context 'when the user is logged in' do
      it "returns http success" do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        user = FactoryGirl.create(:user)
        sign_in user
        get :cart
        expect(response).to have_http_status(:success)
      end
    end

    context 'when the user is NOT logged in' do
      it "redirects them to the log in page" do
        get :cart
        expect(response).to redirect_to new_user_session_path
      end
    end
  end
end
