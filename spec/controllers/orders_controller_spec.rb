require 'rails_helper'

RSpec.describe OrdersController, type: :controller do

  describe 'index' do
    context 'when the user is logged in' do

      context 'when the user has orders in their history' do
        it 'renders the index view' do
          @request.env["devise.mapping"] = Devise.mappings[:user]
          user = FactoryGirl.create(:user)
          sign_in user
          expect(response).to be_success
        end

        it 'assigns the user variable' do
          @request.env["devise.mapping"] = Devise.mappings[:user]
          user = FactoryGirl.create(:user)
          sign_in user
          get :index
          assigns(user).should eq @user
        end

        it 'assigns order variables' do
          @request.env["devise.mapping"] = Devise.mappings[:user]
          user = FactoryGirl.create(:user)
          sign_in user
          orders = Order.create(user_id: user.id)
          get :index
          assigns(orders).should eq @orders
        end
      end
    end

    context 'when the user is NOT logged in' do
      it 'redirects them to the log in page' do
        get :index
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe 'show' do
    context 'when the user is logged in' do
      context 'when the order belongs to the user' do
        it 'assigns the order variable' do
          @request.env["devise.mapping"] = Devise.mappings[:user]
          user = FactoryGirl.create(:user)
          order = FactoryGirl.create(:order, user: user)
          sign_in user
          get :show, params = {id: order.id}
          assigns(order).should eq @order
        end
      end

      context 'when the order does NOT belong to the user' do
        it 'redirects to the order history' do
          @request.env["devise.mapping"] = Devise.mappings[:user]
          user  = FactoryGirl.create(:user)
          user2 = FactoryGirl.create(:user, email: 'bob@example.com')
          order = FactoryGirl.create(:order, user: user)
          sign_in user2
          get :show, params = {id: order.id}
          expect(response).to redirect_to new_user_session_path
        end
      end
    end
  end
end
