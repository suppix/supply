require 'rails_helper'

RSpec.describe DashboardController, type: :controller do

  describe 'GET #home' do
    context 'when the user is logged in' do
      it "redirect to root page" do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        user = FactoryGirl.create(:user)
        sign_in user

        get :home
        expect(response).to be_success
      end
    end

    context 'when the user is NOT logged in' do
      it 'redirects them to the log in page' do

        get :home
        expect(response).to redirect_to new_user_session_path
      end
    end
  end
end
