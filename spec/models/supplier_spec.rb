require 'rails_helper'

RSpec.describe Supplier, type: :model do
  it 'has a valid factory' do
    expect(FactoryGirl.create(:supplier)).to be_valid
  end

  it 'is invalid without a name' do
    expect(FactoryGirl.build(:supplier, name: '')).to_not be_valid
  end

  it 'is invalid without a email' do
    expect(FactoryGirl.build(:supplier, email: '')).to_not be_valid
  end

  it 'is invalid without a address' do
    expect(FactoryGirl.build(:supplier, address: '')).to_not be_valid
  end
end
