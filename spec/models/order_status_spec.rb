require 'rails_helper'

RSpec.describe OrderStatus, type: :model do
	it 'has a valid factory' do
    expect(FactoryGirl.create(:order_status)).to be_valid
  end

  it 'is invalid without a name' do
    expect(FactoryGirl.build(:order_status, name: '')).to_not be_valid
  end
end
