require 'rails_helper'

RSpec.describe Subcategory, type: :model do
	it 'has a valid factory' do
    expect(FactoryGirl.create(:subcategory)).to be_valid
  end

  it 'is invalid without a name' do
    expect(FactoryGirl.build(:subcategory, name: '')).to_not be_valid
  end

  it 'has the ability to be assigned category' do
    nicknacks   = FactoryGirl.create(:category,    name: 'nicknacks')
    subcategory = FactoryGirl.create(:subcategory, name: 'subcategory', category: nicknacks)
    expect(subcategory.category.name).to eq nicknacks.name
  end
end