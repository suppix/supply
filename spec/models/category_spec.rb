require 'rails_helper'

RSpec.describe Category, type: :model do
  it 'has a valid factory' do
    expect(FactoryGirl.create(:category)).to be_valid
  end

  it 'is invalid without a name' do
    expect(FactoryGirl.build(:category, name: '')).to_not be_valid
  end

  it 'destroy category' do
  	category         = FactoryGirl.create(:category)
  	subcategory      = FactoryGirl.create(:subcategory, category_id: category.id)
  	category.destroy

  	expect(subcategory.category.name).to eq 'Default Category'
  end
end
