require 'rails_helper'

RSpec.describe Order, type: :model do
	before(:each) do
    @user = FactoryGirl.create(:user)
  end

  it 'has a valid factory' do
    expect(FactoryGirl.build(:order, user: @user)).to be_valid
  end

  it 'is invalid without a user' do
    expect(FactoryGirl.build(:order, user: nil)).to_not be_valid
  end
end
