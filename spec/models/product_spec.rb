require 'rails_helper'

RSpec.describe Product, type: :model do
  it 'has a valid factory' do
    expect(FactoryGirl.create(:product)).to be_valid
  end

  it 'is invalid without a name' do
    expect(FactoryGirl.build(:product, name: '')).to_not be_valid
  end

  it 'is invalid without a measurement' do
    expect(FactoryGirl.build(:product, measurement: nil)).to_not be_valid
  end

  it 'is invalid without a subcategory' do
    expect(FactoryGirl.build(:product, subcategory: nil)).to be_valid
  end

  it 'is invalid without a supplier' do
    expect(FactoryGirl.build(:product, supplier: nil)).to be_valid
  end

  it 'has the ability to be assigned subcategory' do
    nicknacks = FactoryGirl.create(:subcategory, name: 'nicknacks')
    product   = FactoryGirl.create(:product, subcategory: nicknacks)
    expect(product.subcategory.name).to eq nicknacks.name
  end
end
