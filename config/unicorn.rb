working_directory "/home/deploy/supply/current" 

pid "/home/deploy/supply/shared/pids/unicorn.pid" 

stderr_path "/home/deploy/supply/shared/log/unicorn.log" 
stdout_path "/home/deploy/supply/shared/log/unicorn.log" 

listen "/home/deploy/supply/shared/system/unicorn.supply.sock" 

worker_processes 2

timeout 30
