# config valid only for current version of Capistrano
lock '3.8.0'

set :application, 'supply'
set :repo_url, 'git@bitbucket.org:Dima_Chornenky/supply.git'

#set :branch, "master"
set :deploy_to, "/home/deploy/supply"
set :unicorn_conf, "#{deploy_to}/current/config/unicorn.rb"
set :unicorn_pid, "#{deploy_to}/shared/pids/unicorn.pid"
# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, %w{config/database.yml}
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
set :linked_dirs, %w{bin log pids system tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/uploads}
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5
namespace :deploy do
  task :restart do
  on roles :all do
  execute "if [ -f '/home/deploy/supply/shared/pids/unicorn.pid' ] && [ -e /proc/$(cat '/home/deploy/supply/shared/pids/unicorn.pid') ]; then kill -9 `cat /home/deploy/supply/shared/pids/unicorn.pid`; cd /home/deploy/supply/current && bundle exec unicorn_rails -E production -c config/unicorn.rb -D; else cd /home/deploy/supply/current && bundle exec unicorn_rails -E production -c config/unicorn.rb -D; fi"
  end
  on roles :all do
  execute "cd /home/deploy/supply/current && rake assets:precompile"
  end
  end
  after :publishing, :restart
end
