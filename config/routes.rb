Rails.application.routes.draw do

  namespace :admin do
    resources :categories
    resources :subcategories
    resources :products
    resources :suppliers
    resources :users

    get 'restore_product',  to: 'products#restore',  as: :restore_product
    get 'restore_supplier', to: 'suppliers#restore', as: :restore_supplier

    get 'remove_characteristic', to: 'products#remove_characteristic', as: :remove_characteristic
    get 'remove_value',          to: 'products#remove_value',          as: :remove_value
  end

  get 'admin/home'

  get 'profile',     to: 'account#profile',     as: :profile
  get 'orders_list', to: 'account#orders_list', as: :orders_list
  get 'cart',        to: 'account#cart',        as: :cart

  get 'add_to_cart', to: 'cart#add_to_cart',    as: :add_to_cart

  get 'thanks_for_signing_up', to: 'dashboard#thanks_for_signing_up', as: :thanks_for_signing_up

  get    'remove_item_order', to: 'orders#remove_item',  as: :remove_item_order
  post   'add_item',          to: 'orders#add_item',     as: :add_item
  post   'send_order',        to: 'orders#send_order',   as: :send_order
  post   'resend_order',      to: 'orders#resend_order', as: :resend_order

  get 'search', to: 'products#search', as: :search

  resources :orders
  resources :subcategories, only: :show
  resources :products, only: :show

  devise_for :users, :controllers => { registrations: "registrations", passwords: "passwords", sessions: "sessions" }

  resource :carts, only: [ :update, :show, :destroy ] do
    member do
      put :remove_item
    end
  end

  root 'dashboard#home'
end
