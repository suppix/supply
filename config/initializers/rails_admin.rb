RailsAdmin.config do |config|

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  ## == Cancan ==
  config.authorize_with :cancan

  config.included_models = ["User",
                            "Role",
                            "Category",
                            "Subcategory",
                            "Product",
                            "Characteristic",
                            "Value",
                            "Order",
                            "OrderItem",
                            "OrderStatus",
                            "Supplier"]

  config.actions do
    dashboard
    index
    new do
      except ['OrderStatus', 'Order', 'User', 'OrderItem', 'Role']
    end
    export do
      only ['Order']
    end
    bulk_delete
    show
    edit do
      except ['OrderStatus', 'Role']
    end
    delete do
      except ['OrderStatus', 'Role']
    end
    #show_in_app

    config.model Role do
      list do
        field :name
      end

      show do
        field :name
        field :users
      end
    end

    config.model User do
      parent Role
      edit do
        field :first_name
        field :last_name
        field :phone
        field :active_account
        field :roles
      end

      list do
        field :email
        field :first_name
        field :last_name
        field :phone
        field :active_account
        field :roles
      end

      show do
        field :first_name
        field :last_name
        field :phone
        field :active_account
        field :roles
      end
    end

    config.model Category do
      edit do
        field :name
        field :image
        field :subcategories
      end

      list do
        field :name
        field :image
        field :subcategories
      end

      show do
        field :name
        field :image
        field :subcategories
      end
    end

    config.model Subcategory do
      parent Category
      edit do
        field :name
        field :image
        field :category
      end

      list do
        field :name
        field :image
        field :category
      end

      show do
        field :name
        field :image
        field :products
        field :category
      end
    end

    config.model Product do
      parent Subcategory
      edit do
        field :name
        field :photo
        field :measurement
        field :description
        field :subcategory
        field :supplier
        field :characteristics
      end

      list do
        field :name
        field :photo
        field :measurement
        field :subcategory
        field :supplier
      end

      show do
        field :name
        field :photo
        field :measurement
        field :description
        field :subcategory
        field :supplier
      end
    end

    config.model Characteristic do
      edit do
        field :name
        field :values
      end

      list do
        field :name
        field :values
      end

      show do
        field :name
        field :values
      end
    end

    config.model Value do
      parent Characteristic
      edit do
        field :name
      end

      list do
        field :name
      end

      show do
        field :name
      end
    end

    config.model Supplier do
      edit do
        field :name
        field :email
        field :address
        field :phone
        field :products
      end

      list do
        field :name
        field :email
        field :address
        field :phone
        field :products
      end

      show do
        field :name
        field :email
        field :address
        field :phone
        field :products
      end
    end

    config.model OrderStatus do
      edit do
        field :name
      end

      list do
        field :name
      end

      show do
        field :name
      end
    end

    config.model Order do
      parent OrderStatus
      edit do
        field :order_status
        field :user
        field :address
        field :order_items
      end

      list do
        field :order_status
        field :user do
          pretty_value do
            bindings[:object].id
            path = bindings[:view].show_path(model_name: 'User', id: bindings[:object].user_id)
            bindings[:view].tag(:a, href: path) << User.find(bindings[:object].user_id).email
          end
        end
        field :address
        field :order_items
      end

      show do
        field :order_status
        field :address
        field :created_at do
          date_format :short
        end
        field :updated_at do
          date_format :short
        end
        field :user do
          pretty_value do
            path = bindings[:view].show_path(model_name: 'User', id: bindings[:object].user_id)
            bindings[:view].tag(:a, href: path) << User.find(bindings[:object].user_id).email
          end
        end
        field :order_items
      end
    end

    config.model OrderItem do
      parent Order
      exclude_fields_if do
        type == :datetime
      end
    end

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end
end
